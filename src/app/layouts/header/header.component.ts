import { Component, OnInit } from '@angular/core';
import { LayoutsService } from '../layouts.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public layoutService: LayoutsService
  ) { }

  ngOnInit(): void {
  }

}
