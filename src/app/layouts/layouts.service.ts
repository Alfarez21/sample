import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LayoutsService {

  public isLogined: boolean = false;
  constructor(
    private router: Router
  ) { }

  // ============================ //
  // LOGIN
  // ============================ //
  login(data:any){
    console.log(data)
    if(data.email=="demo@gmail.com"&&data.pass=="demo"){
      alert("login berhasil")
      this.isLogined = true;
      this.router.navigateByUrl("/")
    }else{
      alert("gagal login")
    }
  }

  logout(){
    this.isLogined = false;
    this.router.navigateByUrl("/")
  }
}

