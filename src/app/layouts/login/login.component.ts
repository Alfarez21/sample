import { Component, OnInit, TRANSLATIONS } from '@angular/core';
import { LayoutsService } from '../layouts.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string = "";
  public pass: string = "";

  constructor(
    private layoutService: LayoutsService
  ) { }

  ngOnInit(): void {
  }

  // ===================== //
  // Login
  // ===================== //
  login(){
    const data = {
      email: this.email,
      pass: this.pass
    }
    this.layoutService.login(data)
  }
}
