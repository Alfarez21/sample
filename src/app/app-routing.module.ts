import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutsComponent } from './layouts/layouts.component';
import { LoginComponent } from './layouts/login/login.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { 
    path:"", component:LayoutsComponent, children: [
      { path: "", component: HomeComponent},
      { path: "about", component: AboutComponent},
      { path: "contact", component: ContactComponent}
    ]
  },

  { path:"login", component:LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
