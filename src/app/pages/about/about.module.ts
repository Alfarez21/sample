import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about.component';
import { MyCompModule } from 'src/app/components/my-comp/my-comp.module';



@NgModule({
  declarations: [
    AboutComponent
  ],
  imports: [
    CommonModule,
    MyCompModule
  ]
})
export class AboutModule { }
